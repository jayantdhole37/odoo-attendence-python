import pandas as pd
import os
import subprocess
import argparse
import sys
import math
import xmlrpc.client

#Error messages list
ERR_MSG_ID_FILE = "INVALID ID FILE"
ERR_MSG_ATT_FILE = "INVALID ATTENDANCE FILE"
ERR_MSG_INCORRECT = "INCORRECT ATTENDANCE FILE OR ID FILE"

if len(sys.argv) != 11:
    print("Usage:: python3 <python application file.py> --database <database name> --username <username> --password <password> --host <host> <attendance file.dat> <id file.dat>")
    quit()



#TAKE ATTENDANCE FILE AND ID FILE FROM COMMANDLINE
ap=argparse.ArgumentParser()
ap.add_argument('-db', '--database', action='store', dest='database', help='Enter Database Name.')
ap.add_argument('-user', '--username', action='store', dest='username', help='Enter Username.')
ap.add_argument('-pass', '--password', action='store', dest='password', help='Enter password.')
ap.add_argument('-host', '--host', action='store', dest='host', help='Enter Hostname')
ap.add_argument(dest="attendance_file", help="GET ATTENDANCE file")
ap.add_argument(dest="id_file", help="GET ID MATCH file")
args = ap.parse_args()

database = args.database
username = args.username
password = args.password
host = args.host
port = "8069"

try:
    URL_COMMON = "{}:{}/xmlrpc/2/common".format(host, port)
    URL_OBJECT = "{}:{}/xmlrpc/2/object".format(host, port)
    ODOO_COMMON = xmlrpc.client.ServerProxy(URL_COMMON)
    ODOO_OBJECT = xmlrpc.client.ServerProxy(URL_OBJECT)
    UID = ODOO_COMMON.authenticate(database, username, password, {})
    if str(UID) == "False":
        print("Odoo authentication error! please check all odoo parameters")
        quit()
except:
    print("Odoo authentication error!please check all parameters")
    quit()

attendance_file = args.attendance_file
id_file = args.id_file

#MAP BIOMETRIC-ID AND ODOO-ID in the ID file
try:
    dmap = pd.read_csv(args.id_file, sep='\t')
    map1 = dict(zip(dmap["bioid"],dmap["odooid"]))
except:
    print(ERR_MSG_ID_FILE)
    quit()


#READ ATTENDENCE FILE
df = pd.read_csv(args.attendance_file, sep='\t', header=None)

df[1] = pd.to_datetime(df[1])    #CONVERT DATE COLUMN TO DATETIME FORMAT

try:
    df['odooid'] = df[0].map(map1)    #ADD ODOO-ID COLUMN TO THE DATAFILE
    for i in range(0,len(df['odooid'])):
        check1 = df['odooid'].iloc[i]
        if math.isnan(check1) == True :
            print("No odoo ID for Bio ID",df[0].iloc[i])
            quit()
except:
    print(ERR_MSG_INCORRECT)
    quit()



ids = df.odooid.unique()            #GET ALL ODOOIDS

for i in range(0,len(ids)):     #SELECT A ID FROM ids LIST
    if(ids[i] == 0):
        print("Entry not registered in odoo")
    else:
        tempid = df.loc[df['odooid'] == ids[i]]     #GET  ALL RECORD FOR THAT ID
        j=0
        l = len(tempid)                                 #GET TOTAL NUMBER OF RECORDS
        while j < l:
            q = tempid[1].iloc[j]
            p = pd.to_datetime(q).date()                #GET DATE
            p = pd.to_datetime(p)
            p1 = p + pd.Timedelta(days=1)
            p1 = pd.to_datetime(p1)
            sort = tempid.loc[(tempid[1] >= p) & (tempid[1] < p1)]          #GET RECORDS OF THAT DATE
            start = min(sort[1])                                       #GET 1ST RECORD (INTIME)
            start1 = start.date().strftime('%m/%d/%Y')                          #GET START DATE
            start2 = start.time()                                               #GET START TIME
            end = max(sort[1])                                        #GET LAST RECORD (OUTTIME)
            end1 = end.date().strftime('%m/%d/%Y')                              #GET END DATE
            end2 = end.time()                                                   #GET END TIME
            add = len(sort)                                                     #GET NUMBER OF RECORDS BETWEEN 1ST AND LAST RECORD
            if start == end:
                {
                    subprocess.check_call(["python3", "./single.py", "--database", database, "--username", username, "--password", password, "--host", host, str(ids[i]), str(start1), str(start2)])        #ADD RECORD FOR IN     (Edit location if file position changed ,same for double.py)
                }
            else:
                {
                    #ADD RECORD FOR BOTH IN AND OUT
                    subprocess.check_call(["python3", "./double.py", "--database", database, "--username", username, "--password", password, "--host", host, str(ids[i]), str(start1), str(start2), str(end1), str(end2)])
                }
            j = j + add             #GO TO NEXT DATE



