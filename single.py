import xmlrpc.client
import datetime
from datetime import timedelta
import argparse

class Odoo() :
    def __init__(self,db, user, pas, host):
        """
            Create
            Read
            Update
            Delete
        """
        self.DATA = db # db name                   #ODOO DATABASE DETAILS
        self.USER = user # email address
        self.PASS = pas # password
        self.PORT = "8069" # port
        self.URL  = host # base url
        self.URL_COMMON = "{}:{}/xmlrpc/2/common".format(
            self.URL, self.PORT)
        self.URL_OBJECT = "{}:{}/xmlrpc/2/object".format(
            self.URL, self.PORT)

    def authenticateOdoo(self):                                             #Authenticate ODOO
        self.ODOO_COMMON = xmlrpc.client.ServerProxy(self.URL_COMMON)
        self.ODOO_OBJECT = xmlrpc.client.ServerProxy(self.URL_OBJECT)
        self.UID = self.ODOO_COMMON.authenticate(
            self.DATA
            , self.USER
            , self.PASS
            , {})

    def partnerAdd(self, partnerRow):                                       #ADD ATTENDANCE  RECORD
        partner_id = self.ODOO_OBJECT.execute_kw(
            self.DATA
            , self.UID
            , self.PASS
            , 'hr.attendance'
            , 'create'
            , partnerRow)
        return partner_id

def main():

    ap=argparse.ArgumentParser()
    ap.add_argument('-db','--database', action='store', dest='database', help='Enter Database Name.')
    ap.add_argument('-user','--username', action='store', dest='username', help='Enter Username.')
    ap.add_argument('-pass','--password', action='store', dest='password', help='Enter password.')
    ap.add_argument('-host','--host', action='store', dest='host',help='Enter Hostname')
    ap.add_argument('id',help = 'Attendance id')
    ap.add_argument('indate',help = 'Check-IN date',)
    ap.add_argument('intime',help= 'Check-IN time', )
    args = ap.parse_args()

    database = args.database
    username = args.username
    password = args.password
    host = args.host

    od = Odoo(database,username,password,host)
    od.authenticateOdoo()

    a=args.indate+" "+args.intime

    date1 = datetime.datetime.strptime(a, "%m/%d/%Y %H:%M:%S")              #CONVERT TO APPROPRIATE FORMAT




    partnerRow = [{                                                         #RECORD DETAILS
        "employee_id": args.id
        , "check_in": date1-timedelta(hours=5,minutes=30)
    }]
    try:
       od.partnerAdd(partnerRow)
       print("Employee ID:"+args.id+" Check-IN:",date1)
       print("done")
    except:
       print("Record already Exists!")

if __name__ == '__main__':
    main()

